package be.kdg.java2;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        IntegerPrinter integerPrinter = new IntegerPrinter(13);
//        integerPrinter.print();
//        StringPrinter stringPrinter = new StringPrinter("Hello");
//        stringPrinter.print();
//        ObjectPrinter integerPrinter = new ObjectPrinter(13);
//        integerPrinter.setIt("hello");
//        integerPrinter.print();
//        ObjectPrinter stringPrinter = new ObjectPrinter("Hello");
//        stringPrinter.print();
//        GenericPrinter<Integer> integerPrinter = new GenericPrinter<>(13);
//        //integerPrinter.setIt("Hello");
//        integerPrinter.print();
//        GenericPrinter<String> stringPrinter = new GenericPrinter<>("Hello");
//        stringPrinter.print();
//
//        ArrayList stringList = new ArrayList();
//        stringList.add("hello");
//        stringList.add(1234);
//        String inhoud = (String) stringList.get(1);

        GenericPrinter<Cat> catPrinter = new GenericPrinter<>(new Cat());
        catPrinter.setIt(new Cat());
        catPrinter.print();

//        List<String> strings = new ArrayList<>();
//        strings.add("hello");
//        printList(strings);
        List<Cat> cats = new ArrayList<>();
        cats.add(new Cat());
        printList(cats);
     }

    public static void printList(List<? extends Animal> list){
        System.out.println(list);
    }

    //Generic method
    public static <T, V> T questionIt(T it, V it2){
        System.out.println(it + "?" + it2);
        return it;
    }
}
