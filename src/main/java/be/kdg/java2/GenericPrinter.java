package be.kdg.java2;

public class GenericPrinter<T extends Animal> {
    private T it;

    public GenericPrinter(T it) {
        this.it = it;
    }

    public void setIt(T it) {
        this.it = it;
    }

    public T getIt() {
        return it;
    }

    public void print(){
        System.out.println(it + "!");
        it.eat();
    }
}
