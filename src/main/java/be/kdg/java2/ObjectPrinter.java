package be.kdg.java2;

public class ObjectPrinter {
    private Object it;

    public ObjectPrinter(Object it) {
        this.it = it;
    }

    public void setIt(Object it) {
        this.it = it;
    }

    public void print(){
        System.out.println(it + "!");
    }
}
