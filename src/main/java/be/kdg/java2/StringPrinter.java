package be.kdg.java2;

public class StringPrinter {
    private String it;

    public StringPrinter(String it) {
        this.it = it;
    }

    public void setIt(String it) {
        this.it = it;
    }

    public void print(){
        System.out.println(it + "!");
    }
}
