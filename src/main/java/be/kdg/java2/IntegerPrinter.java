package be.kdg.java2;

public class IntegerPrinter {
    private int number;

    public IntegerPrinter(int number) {
        this.number = number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void print(){
        System.out.println(number + "!");
    }
}
