# demogenerics

Demo week2 generics
1) IntegerPrinter en StringPrinter: dubbele code
2) Opgelost met ObjectPrinter? Ok, maar geen type safety
3) Opgelost met GenericPrinter! Geen dubbele code meer en bovendien type safety: bij declaratie leg ik vast wat het type is en vanaf dan kan er niets anders in de klasse gestopt worden (wordt at compile time gecheckt...)
4) Bounded generics: ik kan T extends Animal doen: nu enkel nog voor subklasses van Animal
5) Je kan ook T extends Animal & Comparable doen: T erft van Animal en implementeert Comparabel
6) Generic method: zet <T> voor de returntype van de method
7) Je kan meerdere types specifiëren (<T,V>)
8) Wildcards: List<Cat> erft niet van List<Animal>, je kan die dus niet polymorf gebruiken
9) Oplossing: List<?> nu kan je wel List<Animal> of List<String> gebruiken
10) Je kan wildcards ook bounden: List<? extends Animal>